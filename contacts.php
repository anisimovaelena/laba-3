<?php
    session_start(); 
  ?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Контакты</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="css/main.css">
</head>
<body>
<?php require_once('C:\xampp\htdocs\test\app\header.php') ?>
        <div class="container">
            <div class="row">
                <div class="contact__zav">

                <h1>Контакты </h1> <br> 
                <h2>Заведущая - Юлия Николаевна Иванова</h2> <br>
                
                    +89047672820, 45-45-45 <br>
                    zav.sad@gmail.com <br>
                    Кабинет 102 
                </div>
            </div>
            <div class="row">
                <h2>Закажите звонок - мы перезвоним вам и согласуем время встречи</h2>
                <form>
                    <div class="form-group">
                      <label for="login">Ваше имя</label>
                      <input type="name" class="form-control" placeholder="Ваше имя">
                      <small id="emailHelp" class="form-text text-muted">Как к вам обращаться?</small>
                    </div>
                    <div class="form-group">
                      <label for="exampleInputPhone">Номер телефона</label>
                      <input type="tel" class="form-control" id="exampleInputPhone" placeholder="Номер телефона">
                    </div>
                    <div class="form-check">
                      <input type="checkbox" class="form-check-input" id="exampleCheck1">
                      <label class="form-check-label" for="exampleCheck1">Ответный звонок с 9 до 19 в будни</label>
                    </div>
                    <button type="submit" class="btn btn-primary">Заказать звонок</button>
                  </form>
            </div>
        </div>
    

    <footer class="footer">
        <div class="container-fluid">
           <div class="row">
               
                   <div class="footer__text">Детский сад №201 "Волшебный замок"</div>
               
           </div>
        </div>
    </footer>
</body>
</html>