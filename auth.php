 <?php
    session_start(); 

    if (isset($_SESSION['user'])) {
      header('Location: app/table.php');
  }?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Главная</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" 
    integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="css/main.css">

</head>
<body>
<?php require_once('app\header.php') ?>

        <div class="container">
            <div class="row">
                <form action="app\checkAuth.php" method="post">
                    <div class="form-group">
                      <label for="exampleInputEmail1">Email address</label>
                      <input type="login" class="form-control" name="login" aria-describedby="emailHelp">
                    </div>
                    <div class="form-group">
                      <label for="exampleInputPassword1">Пароль</label>
                      <input type="password" class="form-control" name="password">
                    </div>
                    
                    <button type="submit" class="btn btn-primary">Submit</button>
                    <?php
                      if (isset($_SESSION['message'])) { // isset чтобы не выдавало ошибок о несуществующей переменной Undefined index: user in C:\xampp\htdocs\test\auth.php on line 4
                          echo '<p class="msg"> ' . $_SESSION['message'] . ' </p>';
                      }
                      unset($_SESSION['message']);
                    ?>
                </form>
                
            </div>
        </div>
        <footer class="footer">
            <div class="container-fluid">
               <div class="row">
                   
                       <div class="footer__text">Детский сад №201 "Волшебный замок"</div>
                   
               </div>
            </div>
        </footer>
   </body>
</html>