<?php
session_start();
if (!isset($_SESSION['user'])) { //если глобальная переменая session - пуста (авторизации не было) - то переход на форму авторизации
    header('Location: ../auth.php');
}
require_once 'connect.php';
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Главная</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" 
    integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="../css/main.css">

</head>
<body>


<?php require_once('header.php'); 
require_once('connect.php');
?>


<table class="table">
    <tr >
    <th scope="col" >Номер</th>
    <th scope="col" >Название</th>
    <th scope="col">Кол-во человек в группе</th>
    </tr>
    <?php

            /*
             * Делаем выборку всех строк из таблицы "groups"
             */

            $groups = mysqli_query($connect, "SELECT * FROM `groups`");

            /*
             * Преобразовываем полученные данные в нормальный массив
             */

            $groups = mysqli_fetch_all($groups);

            /*
             * Перебираем массив и рендерим HTML с данными из массива
             * Ключ 0 - id
             * Ключ 1 - name
             * Ключ 2 - children
             
             */

             foreach ($groups as $groups) {
                ?>
                    <tr>
                        <td scope="row"><?= $groups[0] ?></td>
                        <td><?= $groups[1] ?></td>
                        <td><?= $groups[2] ?></td>
                        
                        
                    </tr>
                <?php
            } 
        ?>
</table>

<a class="btn btn-warning" href="logout.php">
Выход
</a>

<footer class="footer">
         <div class="container-fluid">
            <div class="row">
                
                    <div class="footer__text">Детский сад №201 "Волшебный замок"</div>
                
            </div>
         </div>
     </footer>
</body>
</html>