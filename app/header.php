<header class="header">
        <div class="container">
            <div class="row">
                
                <div class="col-lg-2 logo_img">
                    <img src="../img/—Pngtree—green palace_4842546.png" alt="Логотип - замок">
                </div>
                <div class="col-lg-2 sad_hame">
                    <h3>Волшебный замок</h3>
                </div>
                <div class="col-lg-6 ml-auto">
                    <nav>
                        <ul class="menu d-flex justify-content-center">
                            <li class="menu__item">
                                <a class="btn btn-outline-primary" href="http://localhost/test/auth.php">
                                <p><?= isset($_SESSION['user']['login']) ? $_SESSION['user']['login']: 'Войти' ?></p>
                            </a>
                            </li>
                            <li class="menu__item">
                                <a href="http://localhost/test/index.php">Главная</a>
                            </li>
                            <li class="menu__item">
                                <a href="http://localhost/test/group.php">Группы</a>
                            </li>
                            <li class="menu__item">
                                <a href="http://localhost/test/contacts.php">Контакты</a>
                            </li>
                        </ul>
                    </nav>
                </div>
            </div>
    </header>