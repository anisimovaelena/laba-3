<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Главная</title>
    <link rel="stylesheet" href="css/bootstrap.min.css" >
    <!-- <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css"> -->
    <link rel="stylesheet" href="css/main.css">

</head>
<body>
<style>


</style>
<?php require_once('app\header.php') ?>
<?php require_once('contents.php') ?>


    <div class="container">
        <div class="row">
        <div class="table_of_contents">
        <p>Содержание:</p> 
        <?php
                    $text3="<h1>PHP</h1>
                    <p>PHP (/pi:.eɪtʃ.pi:/ англ. PHP: Hypertext Preprocessor — «PHP: препроцессор гипертекста»; первоначально PHP/FI (Personal Home Page / Form Interpreter), а позже названный Personal Home Page Tools[13] — «Инструменты для создания персональных веб-страниц») — скриптовый язык[14] общего назначения, интенсивно применяемый для разработки веб-приложений. 
                    </p>
                    <h2>Область применения</h2>
                    <p>В области веб-программирования, в частности серверной части, PHP — один из популярных сценарных языков (наряду с JSP, Perl и языками, используемыми в ASP.NET).
                    </p> <br>
                    
                    <h3>Крупнейшие сайты</h3>
                    <p> К крупнейшим сайтам, использующим PHP, относятся Facebook, Wikipedia, Yahoo!, Baidu, Tumblr, ВКонтакте, Badoo, Мамба, Mailchimp, Flickr и др</p>
                    <h2>Дополнительные возможности</h2>
                    <p>Язык автоматически поддерживает HTTP Cookies в соответствии со стандартами Netscape.</p>
                    <h3>Создание GUI-приложений</h3>
                    <p>PHP не ориентирован на создание десктопных приложений, но есть потребность в создании интерфейсов для настройки серверов, беспрерывного выполнения, отладки скриптов (сценариев), управления локальными и тестовыми серверами, и т. п. Из-за этого и возникли решения данной проблемы.</p>
                    <h4>Расширения</h4>
                    <ul>
                    <li>День города отмечается в первое воскресенье сентября.</li>
                    <li>2 февраля — день капитуляции немецкой группировки в Сталинградской битве (в 1943 году)</li>
                    <li>12 сентября — день памяти духовного покровителя города Александра Невского, по одной из версий отравленного в столице улуса Джучи Сарай-Берке (сейчас село Царев Волгоградской области)</li>
                    </ul>
                    ";
                    table_content($text3);
                    ?>
                    
            </div>
            </div>
            
        </div>    
    </div>
    <?php require_once('app\footer.php') ?>
</body>
</html>